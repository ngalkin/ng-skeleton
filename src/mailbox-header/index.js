import ng from 'angular';

import MailboxHeaderComponent from './component';

export default ng.module('app.', [])
  .component('mailboxHeader', MailboxHeaderComponent)
  .name;
