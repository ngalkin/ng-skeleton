import ng from 'angular';

import HeroesComponent from './component';
import HeroesListComponent from './list/component';
import HeroesDetailComponent from './detail/component';

import HeroesService from './service';

export default ng.module('app.heroes', [])
  .service('HeroesService', HeroesService)
  .component('heroes', HeroesComponent)
  .component('heroesList', HeroesListComponent)
  .component('heroesDetail', HeroesDetailComponent)
  .name;
