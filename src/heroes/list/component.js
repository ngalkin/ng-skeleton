import template from './heroes.list.html';

class HeroesListComponent {
  constructor(HeroesService) {
    'ngInject';
    this.HeroesService = HeroesService;
  }

  $routerOnActivate() {
    this.HeroesService.list().then(heroes => {
      this.heroes = heroes;
    });
  }
}

export default {
  template,
  controller: HeroesListComponent
};
