export default class HeroesService {

  heroes = [
    { id: 1, name: 'Batman' },
    { id: 2, name: 'Superman' },
    { id: 3, name: 'Ironman' }
  ];

  constructor($q) {
    'ngInject';
    this.$q = $q;
  }

  list() {
    return this.$q.when(this.heroes);
  }

  detail(id) {
    const hero = this.heroes.find(item => item.id === id);
    return this.$q.when(hero);
  }
}
