import template from './heroes.detail.html';

class HeroesDetailComponent {
  constructor(HeroesService) {
    'ngInject';
    this.HeroesService = HeroesService;
  }

  $routerOnActivate(next) {
    this.HeroesService.detail(next.params.id).then(hero => {
      this.hero = hero;
    });
  }
}

export default {
  template,
  controller: HeroesDetailComponent
};
