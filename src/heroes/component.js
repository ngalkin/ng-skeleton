import template from './heroes.html';

export default {
  template,
  $routeConfig: [
    { path: '/', name: 'HeroesList', component: 'heroesList', useAsDefault: true },
    { path: '/:id', name: 'HeroesDetail', component: 'heroesDetail' }
  ]
};
