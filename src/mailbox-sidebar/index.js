import ng from 'angular';

import MailboxSidebarComponent from './component';

export default ng.module('app.navigation', [])
  .component('mailboxSidebar', MailboxSidebarComponent)
  .name;
