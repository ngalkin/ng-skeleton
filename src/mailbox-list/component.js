import template from './mailbox-list.html';

class MailBoxController {
  constructor(MailboxService) {
    'ngInject';
    this.MailboxService = MailboxService;
  }

  $onInit() {
    this.items = [];
    this.MailboxService.list(this.type).then(result => {
      this.items = result;
    });
  }
}


export default {
  template,
  controller: MailBoxController,
  bindings: {
    type: '@'
  }
};
