/* global API */

export default class MailboxService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  list(type) {
    return this.$http.get(`${API}/mailbox/${type}`).then(response => response.data);
  }
}
