import ng from 'angular';

import MailboxListComponent from './component';
import MailboxService from './service';

export default ng.module('app.mailbox.list', [])
  .service('MailboxService', MailboxService)
  .component('mailboxList', MailboxListComponent)
  .name;
