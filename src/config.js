export default ($locationProvider) => {
  'ngInject';
  $locationProvider.html5Mode(false);
};
