import ng from 'angular';

export default ng.module('app.villains', [])
  .component('villains', {
    template: '<h2>Villains</h2><ng-outlet></ng-outlet>',
    $routeConfig: [
      { path: '/', name: 'Villains', component: 'villains', useAsDefault: true }
    ]
  })
  .name;
