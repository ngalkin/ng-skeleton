import ng from 'angular';

import HeaderComponent from './component';

export default ng.module('app.header', [])
  .component('appHeader', HeaderComponent)
  .name;
