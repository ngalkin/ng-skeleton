import ng from 'angular';

import Component from './component';

export default ng.module('app.root', ['ngComponentRouter'])
  .component('app', Component)
  .name;
