import 'font-awesome/less/font-awesome.less';
import 'simple-line-icons/less/simple-line-icons.less';
import 'bootstrap/less/bootstrap.less';
import './assets/css/modern.min.css';

import ng from 'angular';
import '@angular/router/angular1/angular_1_router';

import ngApp from './app';
import ngHeader from './header';
import ngMailboxSidebar from './mailbox-sidebar';
import ngMailboxHeader from './mailbox-header';
import ngMailboxList from './mailbox-list';

// import Config from './config';

ng.module('app', [ngApp, ngHeader, ngMailboxSidebar, ngMailboxHeader, ngMailboxList])
  .value('$routerRootComponent', 'app')
  // .config(Config);
